sap.ui.define([
	], function () {
		"use strict";

		return {
			/**
			 * Rounds the currency value to 2 digits
			 *
			 * @public
			 * @param {string} sValue value to be formatted
			 * @returns {string} formatted currency value with 2 digits
			 */
			currencyValue : function (sValue) {
				if (!sValue) {
					return "";
				}

				return parseFloat(sValue).toFixed(2);
			},
			getCreationDateTime: function(sDate, sTime){
				var oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
				var dateFormatted = "";
				var formattedTime = "";
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance();
				var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({pattern: "HH:mm:ss"});
				if(sDate !== undefined && sDate !== null && sTime !== undefined && sTime !== null){
					dateFormatted = dateFormat.format(sDate);
					var TZOffsetMs = new Date(0).getTimezoneOffset()*60*1000;                             
					formattedTime = timeFormat.format(new Date(sTime.ms + TZOffsetMs));
				}
				// return oBundle.getText("creationDateLbl") + " : " + dateFormatted + " " + formattedTime;
				return dateFormatted + " " + formattedTime;
			},
			getMasterStatus: function(sStatus){
				var oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
				if(sStatus === "A"){
					return oBundle.getText("approved");
				}else if(sStatus === "R"){
					return oBundle.getText("rejected");
				}
				return oBundle.getText("submitted");
			},
			statusText: function(sStatus){
				var oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
				if(sStatus === "A"){
					return oBundle.getText("approved");
				}else if(sStatus === "R"){
					return oBundle.getText("rejected");
				}else if(sStatus === "N"){
					return oBundle.getText("na");
				}
				return oBundle.getText("pending");	
			},
			approvalStatus: function(sStatus){
				if(sStatus === "A"){
					return "Success";
				}else if(sStatus === "R"){
					return "Error";
				}else if(sStatus === "N"){
					return "None";
				}
				return "Warning";	
			}
		};

	}
);
/*global QUnit*/

jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 ApproReqSet in the list

sap.ui.require([
	"sap/ui/test/Opa5",
	"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/pages/App",
	"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/pages/Browser",
	"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/pages/Master",
	"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/pages/Detail",
	"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "com.varian.proapproreq.ProjectAppropriationRequest.view."
	});

	sap.ui.require([
		"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/MasterJourney",
		"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/NavigationJourney",
		"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/NotFoundJourney",
		"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/BusyJourney",
		"com/varian/proapproreq/ProjectAppropriationRequest/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});
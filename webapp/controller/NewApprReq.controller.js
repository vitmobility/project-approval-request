sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/UploadCollectionParameter"
], function (Controller, History, UploadCollectionParameter) {
	"use strict";

	return Controller.extend("com.varian.proapproreq.ProjectAppropriationRequest.controller.NewApprReq", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.varian.proapproreq.ProjectAppropriationRequest.view.NewApprReq
		 */
		onInit: function () {
			this._oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this.getOwnerComponent().getRouter().getRoute("newapproreq").attachPatternMatched(this.initializeNewApproReq, this);
			this.byId("attachments").addEventDelegate({
				onAfterRendering: function () {
					if (this.byId("attachments").getItems().length > 0) {
						this.byId("attachErrorMsgHCP").setVisible(false);
					} else {
						this.byId("attachErrorMsgHCP").setVisible(true);
					}
				}
			}, this);
		},
		initializeNewApproReq: function () {
			// Instantiate model approReq

			var approReq = {};
			approReq.Comments = "";
			approReq.AttachmentSet = [];
			approReq.ApproverSet = [];
			approReq.NoOfApprovers = this._oBundle.getText("approverTabHead", [0]);
			this.getView().setModel(new sap.ui.model.json.JSONModel(approReq), "approReq");
		},
		goToPreviousPage: function () {
			var that = this;
			sap.m.MessageBox.confirm(this._oBundle.getText("navBackConfirmMessage"), {
				title: that._oBundle.getText("confirmationTitle"),
				onClose: function (oAction) {
					if (oAction === sap.m.MessageBox.Action.OK) {
						var oHistory = History.getInstance();
						var sPreviousHash = oHistory.getPreviousHash();
						if (sPreviousHash !== undefined) {
							window.history.go(-1);
						} else {
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							oRouter.navTo("master", true);
						}
					}
				}
			});
		},
		openEmail: function () {
			sap.m.URLHelper.triggerEmail(null, "", "");
		},
		addApprover: function () {
			var approverDetails = {};
			approverDetails.ApprFullname = "";
			approverDetails.ApprId = "";
			approverDetails.ApprEmailId = "";

			var data = this.getView().getModel("approReq").getData();
			data.ApproverSet.push(approverDetails);
			for (var i = 0; i < data.ApproverSet.length; i++) {
				data.ApproverSet[i].SeqNo = i + 1;
			}
			data.NoOfApprovers = this._oBundle.getText("approverTabHead", [data.ApproverSet.length]);
			this.getView().getModel("approReq").setData(data);
		},
		deleteApprover: function (oEvent) {
			var itemIndex = parseInt(oEvent.getParameter("listItem").getBindingContextPath().split("/")[2], 10);
			var data = this.getView().getModel("approReq").getData();
			data.ApproverSet.splice(itemIndex, 1);
			for (var i = 0; i < data.ApproverSet.length; i++) {
				data.ApproverSet[i].SeqNo = i + 1;
			}
			data.NoOfApprovers = this._oBundle.getText("approverTabHead", [data.ApproverSet.length]);
			this.byId("apprList").destroyItems();
			this.getView().getModel("approReq").setData(data);
		},
		suggestApprDetail: function (oEvent) {
			var searchText = oEvent.getParameter("suggestValue");
			var filterName = new sap.ui.model.Filter({
				path: "Fullname",
				operator: sap.ui.model.FilterOperator.EQ,
				value1: searchText
			});
			var aFilters = [];
			aFilters.push(filterName);
			oEvent.getSource().getBinding("suggestionItems").filter(aFilters);
		},
		setApprData: function (oEvent) {
			var parentInp = oEvent.getSource();
			var data = oEvent.getParameter("selectedItem").getModel()
				.getProperty(oEvent.getParameter("selectedItem").getBindingContext().getPath());
			parentInp.setValue(data.Fullname);
			var tabLine = parentInp.getParent();
			var lineContextPath = tabLine.getBindingContextPath();
			var parts = lineContextPath.split("/");
			var lineData = this.getView().getModel("approReq").getData();
			lineData.ApproverSet[parseInt(parts[2], 10)].ApprId = data.Bname;
			lineData.ApproverSet[parseInt(parts[2], 10)].ApprEmailId = data.EmailId;
			this.getView().getModel("approReq").setData(lineData);
		},
		submitApproReq: function () {
			var that = this;
			sap.ui.core.BusyIndicator.show();
			if (this.checkApproReq()) {
				sap.ui.core.BusyIndicator.hide();
				sap.m.MessageBox.confirm(that._oBundle.getText("submitConfirmMessage"), {
					title: that._oBundle.getText("confirmationTitle"),
					onClose: function (oAction) {
						if (oAction === sap.m.MessageBox.Action.OK) {
							sap.ui.core.BusyIndicator.show();
							var approReqData = that.getView().getModel("approReq").getData();
							delete approReqData.NoOfApprovers;
							approReqData.Comments = approReqData.Comments.substring(0, 255);
							approReqData.ApproverSet.forEach(function (oItem) {
								delete oItem.SeqNo;
							});
							approReqData.Ispar = true;
							var serverURL = that.getView().getModel().sServiceUrl;
							var headers = {
								"X-Requested-With": "XMLHttpRequest",
								"Content-Type": "application/json",
								"X-CSRF-Token": "Fetch"
							};
							var oXSRFModel = new sap.ui.model.odata.ODataModel(serverURL, "false", "", "", headers, true);
							sap.ui.getCore().setModel(oXSRFModel, "XCRFTokenModel");
							that.getOwnerComponent().getModel().create("/ApproReqSet", approReqData, {
								success: function (oData) {
									// Now Upload the Files to the Created DMS Document
									that.onStartUpload(oData.DocumentNo, oData.DocumentType, oData.DocumentVer, oData.DocumentPart, oXSRFModel.getSecurityToken());
									//sap.ui.core.BusyIndicator.hide();
								},
								error: function (oError) {
									sap.ui.core.BusyIndicator.hide();
									sap.m.MessageBox.error(that._oBundle.getText("approReqCreateError"), {
										title: that.getView().getModel("i18n").getResourceBundle().getText("errorTitle"),
										details: JSON.parse(oError.responseText).error.message.value,
										onClose: null
									});
								}
							});
						}
					}
				});
			} else {
				sap.ui.core.BusyIndicator.hide();
			}
		},
		// Check the entries in te Project Appropriation Request to be submitted
		checkApproReq: function () {
			// Check Comments
			if (this.byId("comments").getValue() === "" || this.byId("comments").getValue() === undefined || this.byId("comments").getValue() ===
				null) {
				sap.m.MessageToast.show(this._oBundle.getText("infoSectionError"));
				this.byId("approReqTabBar").setSelectedKey("info");
				this.byId("comments").setValueState("Error");
				return false;
			} else {
				this.byId("comments").setValueState("None");
			}

			// Check Approver List
			if (!(this.byId("apprList").getItems().length > 0)) {
				sap.m.MessageToast.show(this._oBundle.getText("noApprFoundError"));
				this.byId("approReqTabBar").setSelectedKey("approvers");
				return false;
			} else {
				// Now check whether Approver Name for all line items has been entered or not
				var apprList = this.byId("apprList").getItems();
				var apprErrFlg = false;
				for (var i = 0; i < apprList.length; i++) {
					if (apprList[i].getCells()[1].getValue() === "") {
						apprList[i].getCells()[1].setValueState("Error");
						apprErrFlg = true;
					} else {
						apprList[i].getCells()[1].setValueState("None");
					}
				}
				if (apprErrFlg) {
					sap.m.MessageToast.show(this._oBundle.getText("apprSectionError"));
					this.byId("approReqTabBar").setSelectedKey("approvers");
					return false;
				}
			}

			// Check Attachments
			if (!(this.byId("attachments").getItems().length > 0)) {
				sap.m.MessageToast.show(this._oBundle.getText("noAttachError"));
				this.byId("approReqTabBar").setSelectedKey("attachmentsFilter");
				return false;
			}
			return true;
		},
		/**
		 * This Section contains methods for File Handling only
		 * 
		 **/
		onChange: function (oEvent) {
			var headers = {
				"X-Requested-With": "XMLHttpRequest",
				"Content-Type": "application/atom+xml",
				"X-CSRF-Token": "Fetch"
			};
			var serviceUrl = this.getOwnerComponent().getModel().sServiceUrl;
			var oModel1 = new sap.ui.model.odata.ODataModel(serviceUrl,
				"false", "", "", headers, true);
			sap.ui.getCore().setModel(oModel1, "XCRFTokenModel");
			sap.ui.getCore().getModel("XCRFTokenModel").refreshSecurityToken();
			//var token = sap.ui.getCore().getModel("XCRFTokenModel").getHeaders()["x-csrf-token"];
			var oRequest = sap.ui.getCore().getModel("XCRFTokenModel")._createRequest();
			var oUploadCollection = oEvent.getSource();
			// Header Token
			var oCustomerHeaderToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: oRequest.headers["x-csrf-token"]
			});
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
			
		},
		onFileDeleted: function (oEvent) {
			sap.m.MessageToast.show(this._oBundle.getText("fileDeletedMsg"));
			if (this.byId("attachments").getItems().length === 0) {
				this.byId("attachErrorMsgHCP").setVisible(true);
			} else {
				this.byId("attachErrorMsgHCP").setVisible(false);
			}
		},
		onFilenameLengthExceed: function (oEvent) {
			sap.m.MessageToast.show(this._oBundle.getText("fileNameLengthExceedMsg"));
		},
		onFileSizeExceed: function (oEvent) {
			sap.m.MessageToast.show(this._oBundle.getText("fileSizeExceedMsg"));
		},
		onTypeMissmatch: function (oEvent) {
			// sap.m.MessageToast.show("Event typeMissmatch triggered");
			sap.m.MessageToast.show(this._oBundle.getText("fileTypeMismatchErr"));
		},
		onStartUpload: function (sDocNo, sDocType, sDocVer, sDocPart, sXCSRFToken) {
			var docData = {
				"DocumentNo": sDocNo,
				"DocumentType": sDocType,
				"DocumentVer": sDocVer,
				"DocumentPart": sDocPart,
				"XCSRFToken": sXCSRFToken
			};
			var docModel = new sap.ui.model.json.JSONModel();
			docModel.setData(docData);
			this.getView().setModel(docModel, "docData");
			var oUploadCollection = this.byId("attachments");
			var cFiles = oUploadCollection.getItems().length;
			var uploadInfo = cFiles + " file(s)";

			if (cFiles > 0) {
				oUploadCollection.upload();
			}
		},
		onBeforeUploadStarts: function (oEvent) {
			var docData = this.getView().getModel("docData").getData();
			var slugValue = docData.DocumentNo + "/" + docData.DocumentType + "/" + docData.DocumentVer + "/" +
				docData.DocumentPart + "/" +
				oEvent.getParameter("fileName");
			// Header Slug
			var oCustomerHeaderSlug = new UploadCollectionParameter({
				name: "slug",
				value: slugValue
			});
			var oCustomerHeaderFileType = new UploadCollectionParameter({
				name: "Accept",
				value: "application/atom+xml"
			});
			// XCSRF Token
			var oCustomerHeaderXCSRFToken = new UploadCollectionParameter({
				name: "x-csrf-token",
				value: docData.XCSRFToken
			});
			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
			oEvent.getParameters().addHeaderParameter(oCustomerHeaderFileType);
			if (oEvent.getParameters().getHeaderParameter("x-csrf-token") === undefined || oEvent.getParameters().getHeaderParameter(
					"x-csrf-token") === null) {
				oEvent.getParameters().addHeaderParameter(oCustomerHeaderXCSRFToken);
			}
		},
		onUploadComplete: function (oEvent) {
			var oUploadCollection = this.byId("attachments");
			// for (var i = 0; i < oUploadCollection.getItems().length; i++) {
			// 	oUploadCollection.removeItem(oUploadCollection.getItems()[i]);
			// }
			oUploadCollection.removeAllItems();
			oUploadCollection.removeAllHeaderParameters();
			oUploadCollection.removeAllParameters();
			sap.ui.core.BusyIndicator.hide();
			var that = this;
			var docData = that.getView().getModel("docData").getData();
			// Get Approvers String
			var sApproverStr = "";
			for (var i = 0; i < that.getView().getModel("approReq").getData().ApproverSet.length; i++) {
				if (sApproverStr === "") {
					sApproverStr = that.getView().getModel("approReq").getData().ApproverSet[i].ApprId;
				} else {
					sApproverStr = sApproverStr + "|" + that.getView().getModel("approReq").getData().ApproverSet[i].ApprId;
				}
			}
			// Trigger Approval Workflow
			that.getView().getModel().callFunction("/TriggerWF", {
				method: "GET",
				urlParameters: {
					"DocumentNo": docData.DocumentNo,
					"DocumentType": docData.DocumentType,
					"DocumentPart": docData.DocumentPart,
					"DocumentVer": docData.DocumentVer,
					"Approvers": sApproverStr
				}
			});

			sap.m.MessageBox.show(that._oBundle.getText("Message.SubmitSuccess", $.trim(docData.DocumentNo)), {
				icon: sap.m.MessageBox.Icon.SUCCESS,
				title: "Success",
				actions: [sap.m.MessageBox.Action.OK],
				onClose: function (oAction) {
					if (sap.m.MessageBox.Action.OK === oAction) {
						that.getView().setModel(new sap.ui.model.json.JSONModel(), "approReq");

						var oHistory = History.getInstance();
						var sPreviousHash = oHistory.getPreviousHash();
						if (sPreviousHash !== undefined) {
							window.history.go(-1);
						} else {
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							oRouter.navTo("master", true);
						}

					}
				}
			});
		},
		openUserSelectDialog: function (oEvent) {
			this.UserSelLineInp = oEvent.getSource();
			if (!this._UserSelVH) {
				this._UserSelVH =
					new sap.ui.xmlfragment("com.varian.proapproreq.ProjectAppropriationRequest.view.UserHelpDialog", this);
				this._UserSelVH.addStyleClass(this.getOwnerComponent().getContentDensityClass());
				this.getView().addDependent(this._UserSelVH);
			}
			var text = oEvent.getSource().getValue();
			if (text !== "") {
				var filterName = new sap.ui.model.Filter({
					path: "Fullname",
					operator: sap.ui.model.FilterOperator.EQ,
					value1: text
				});

				var filters = new sap.ui.model.Filter({
					filters: [filterName],
					and: true
				});
				this._UserSelVH.getBinding("items").filter(filters);
			} else {
				this._UserSelVH.getBinding("items").filter();
			}
			this._UserSelVH.open();
		},
		handleUserSearch: function (oEvent) {
			var text = oEvent.getParameter("value");
			var filterName = new sap.ui.model.Filter({
				path: "Fullname",
				operator: sap.ui.model.FilterOperator.Contains,
				value1: text
			});

			var filters = new sap.ui.model.Filter({
				filters: [filterName],
				and: false
			});
			oEvent.getParameter("itemsBinding").filter(filters);
		},
		onUserSelect: function (oEvent) {
			var parentInp = this.UserSelLineInp;
			var data = oEvent.getParameter("selectedItem").getModel()
				.getProperty(oEvent.getParameter("selectedItem").getBindingContext().getPath());
			parentInp.setValue(data.Fullname);
			var tabLine = parentInp.getParent();
			var lineContextPath = tabLine.getBindingContextPath();
			var parts = lineContextPath.split("/");
			var lineData = this.getView().getModel("approReq").getData();
			lineData.ApproverSet[parseInt(parts[2], 10)].ApprId = data.Bname;
			lineData.ApproverSet[parseInt(parts[2], 10)].ApprEmailId = data.EmailId;
			this.getView().getModel("approReq").setData(lineData);
		},
		handleUserVHClose: function () {
				this._UserSelVH.close();
			}
			/**
			 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			 * (NOT before the first rendering! onInit() is used for that one!).
			 * @memberOf com.varian.proapproreq.ProjectAppropriationRequest.view.NewApprReq
			 */
			//	onBeforeRendering: function() {
			//
			//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.varian.proapproreq.ProjectAppropriationRequest.view.NewApprReq
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.varian.proapproreq.ProjectAppropriationRequest.view.NewApprReq
		 */
		//	onExit: function() {
		//
		//	}

	});

});